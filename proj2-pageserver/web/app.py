from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route('/<path>')

def hello(path):


    if ("//" in path or ".." in path or "~" in path):
        return error_403(403)

    source_path = "templates/" + path

    if (os.path.isfile(source_path)):
        return render_template(path), 200

    else:
        return error_404(404)

def error_404(error):
    return render_template("404.html"), 404

def error_403(error):
    return render_template("403.html"), 403


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')